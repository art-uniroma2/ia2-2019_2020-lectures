package it.uniroma2.art.teaching.ia2_2019_2020;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class ModelExamples {

	public static void main(String[] args) {
		// Abbiamo bisogno di una ValueFactory per istanziare oggetti di tipo Value (o sue sottoinferfacce)
		ValueFactory vf = SimpleValueFactory.getInstance();

		// Usiamo la value factory per costruire un IRI. � possibile costruire anche literal, bnode e
		// statement (come dimostrato pi� sotto)
		IRI socrates = vf.createIRI("http://example.org/Socrates");

		// Costruisco uno statement che afferma: "socrate � una persona"
		Statement st = vf.createStatement(socrates, RDF.TYPE, FOAF.PERSON);

		// Stampo lo statement di cui sopra
		System.out.println(st);
		System.out.println("---");
		
		// Costruisco un modello, cio� un insieme di statement
		Model myModel = new TreeModel();

		// Aggiunto lo statement creato in precendeza al modello di cui sopra
		myModel.add(st);
		
		// Ripeto i punti di sopra per la risorsa aristotle
		IRI aristotle = vf.createIRI("http://example.org/Aristotle");
		Statement st2 = vf.createStatement(aristotle, RDF.TYPE, FOAF.PERSON);
		myModel.add(st2);
		
		// Stampo il modello
		System.out.println(myModel);
		System.out.println("---");
		
		// Stampo solo le triple aventi socrates come soggetto
		System.out.println(myModel.filter(socrates, null, null));
		System.out.println("---");

		// Stampo il fatto che il modello contenga (true) almeno uno statement avente come soggetto socrates
		System.out.println(myModel.contains(socrates, null, null));
		System.out.println("----");
		// Definisco alcuni namespace
		myModel.setNamespace(FOAF.NS);
		myModel.setNamespace(RDF.NS);
		myModel.setNamespace("", "http://example.org/");
		
		// Stampo i dati in formato TURTLE
		
		// Strada "lunga"
		// Rio.write(myModel, Rio.createWriter(RDFFormat.TURTLE, System.out));
		
		System.out.println(myModel.getNamespaces());
		// Strada "breve"
		Rio.write(myModel, System.out, RDFFormat.TURTLE);
		
		// Sembra esserci un bug per cui i prefissi non sono usati se sono applicabili soltanto ai soggetti
		// delle triple (come nel nostro esempio)
		
		// Se facciamo in modo che il namespace http://example.org/ sia usato anche da qualche oggett, allora
		// troveremo il prefisso nell'output
		
		System.out.println("---");
		// Da notare il metodo di aggiunta di una tripla che prende le tre componenti separatamente (pi� un
		// vararg per i contesti)
		myModel.setNamespace(OWL.NS);
		myModel.add(FOAF.PERSON, OWL.EQUIVALENTCLASS, vf.createIRI("http://example.org/", "MyPerson"));
		Rio.write(myModel, System.out, RDFFormat.TURTLE);

		
	}

}
