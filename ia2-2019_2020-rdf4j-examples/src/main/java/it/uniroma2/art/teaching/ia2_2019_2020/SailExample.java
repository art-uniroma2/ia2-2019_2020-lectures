package it.uniroma2.art.teaching.ia2_2019_2020;

import java.io.File;

import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.inferencer.fc.SchemaCachingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

public class SailExample {
	public static void main(String[] args) {
		// Creo un oggetto MemoryStore (� il Sail che si occupa del vero e proprio storage dei dati)
		MemoryStore memoryStore = new MemoryStore();
		// chiedo che i dati siano salvati sul filesystem (altrimenti i dati sarebbero gestiti soltanto in
		// memoria)
		memoryStore.setPersist(true);
		// specifico il percorso (in questo caso relativo) della cartella dove voglio che i dati vengano
		// salvati
		memoryStore.setDataDir(new File("repository"));

		// Creo un sail che implementa il reasoning RDFS e lo impilo sul memory store
		SchemaCachingRDFSInferencer schemaCachingRdfsInferencer = new SchemaCachingRDFSInferencer(
				memoryStore);

		// Creo un Repository basato sulla pila di Sail costruita in precedenza
		Repository rep = new SailRepository(schemaCachingRdfsInferencer);
		rep.init(); // inizializzo il repository
		try {
			// ottengo una connessione al repository
			try (RepositoryConnection con = rep.getConnection()) {
				// Preparo un update per aggiungere i dati usando SPARQL 1.1
				Update update = con.prepareUpdate("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
						+ "INSERT DATA {\n"
						+ "	<http://example.org/Person> rdfs:subClassOf <http://example.org/Mortal> .\n"
						+ "	<http://example.org/socrates> a <http://example.org/Person> .\n" + "}");
				// eseguo l'update
				update.execute();
			}

			// otttengo un'altra connessione
			try (RepositoryConnection con = rep.getConnection()) {
				// stampo gli statement su socrates...
				
				// ... senza inferenza (vedi il false)
				System.out.println(QueryResults.asList(con.getStatements(
						con.getValueFactory().createIRI("http://example.org/socrates"), null, null, false)));

				System.out.println("---");

				// ...  con inferenza (vedi il true)
				System.out.println(QueryResults.asList(con.getStatements(
						con.getValueFactory().createIRI("http://example.org/socrates"), null, null, true)));

			}
		} finally {
			rep.shutDown(); // spengo il repository
		}
	}
}
