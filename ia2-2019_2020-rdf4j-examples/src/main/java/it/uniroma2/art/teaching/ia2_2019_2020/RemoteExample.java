package it.uniroma2.art.teaching.ia2_2019_2020;

import org.eclipse.rdf4j.repository.http.HTTPRepository;

public class RemoteExample {
	public static void main(String[] args) {
		// Esempio di connession ad un repository remoto
		HTTPRepository rep = new HTTPRepository("http://127.0.0.1:7200/repositories/Test");
		// rep.setUsernameAndPassword(username, password);
		rep.init();
		try {

		} finally {
			rep.shutDown();
		}
	}
}
