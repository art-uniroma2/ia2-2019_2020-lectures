package it.uniroma2.art.teaching.ia2_2019_2020;

import java.net.URISyntaxException;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.eclipse.rdf4j.rio.helpers.BasicWriterSettings;

public class AGROVOCExamples {
	public static void main(String[] args)
			throws RDFHandlerException, UnsupportedRDFormatException, URISyntaxException {
		// Creo uno SPARQLRepository per interagire con lo SPARQL endpoint del tesauro AGROVOC
		Repository rep = new SPARQLRepository("http://agrovoc.uniroma2.it/sparql");
		rep.init(); // inizializzo il repository
		try {
			// Richiedo una connessione al repository. Si noti l'uso del costrutto try-with-resources per
			// assicurarsi che la connessione non � pi� in scope (e pertanto non pu� essere pi� usata)
			try (RepositoryConnection con = rep.getConnection()) {
				// Preparo una tuple query (cio� una SELECT che restituisce una sequenza di tuple)
				TupleQuery query = con.prepareTupleQuery("SELECT * { ?s ?p ?o } limit 10");
				// Faccio il binding di un valore alla variabile s: utile quando il valore � un argomento
				// ottenuto dinamicamente (es. ottenuto come argomento da un chiamate terzo)
				query.setBinding("s",
						con.getValueFactory().createIRI("http://aims.fao.org/aos/agrovoc/c_1071")); // bread

				// Faccio la query ed aggreggo tutti i risultati in una lista (in memoria)
				// System.out.println(QueryResults.asList(query.evaluate()));

				// Pi� esplicitamente. Faccio la query ed itero sui risultati
				// Si noti ancora l'uso del costrutto try-with-resources per chiudere i risultati quando si �
				// finito di usarli
				try (TupleQueryResult result = query.evaluate()) {
					while (result.hasNext()) {
						BindingSet bs = result.next();
						System.out.println(bs.getValue("p") + " " + bs.getValue("o"));
					}
				}

				System.out.println("---");

				// Preparo una boolean query (cio� una ASK che restituisce un valore booleano)
				BooleanQuery query2 = con
						.prepareBooleanQuery("ASK {<http://aims.fao.org/aos/agrovoc/c_1071> ?p ?o }");
				// Eseguo la query booleana
				System.out.println(query2.evaluate());

				System.out.println("---");

				// Preparo una graph query (cio� una DESCRIBE oppure una CONSTRUCT)
				GraphQuery query3 = con
						.prepareGraphQuery("DESCRIBE <http://aims.fao.org/aos/agrovoc/c_1071>");

				// Eseguo la query e colleziono gli statemento in un oggetto model (altrimenti avrei potuto
				// iterare suli statementi restituiti
				Model m = QueryResults.asModel(query3.evaluate());

				// Utilizzer� un ogetto WriterConfig per configurare la generazione della serializzazione
				// TURTLE
				WriterConfig writerConfig = new WriterConfig();
				writerConfig.set(BasicWriterSettings.PRETTY_PRINT, true);
				writerConfig.set(BasicWriterSettings.INLINE_BLANK_NODES, true);

				Rio.write(m, System.out, "", RDFFormat.TURTLE, writerConfig);
			}
		} finally {
			rep.shutDown(); // Spengo il repository
		}
	}
}
