Esempi su Eclipse RDF4J
=======================

Questo progetto contiene alcuni esempi relativi a [Eclipse RDF4J](http://rdf4j.org/).

**Attenzione**: questo file README è scritto in sintassi Markdown, pertanto alcuni simboli e l'identazione di
certe righe sono finalizzati all'impaginazione.

Esempi
------
* ModeleExamples
* AGROVOCExamples
* SailExample
* RemoteExample

Note
----
Si noti che nel file `pom.xml` sono state aggiunte le dipendenze a `rdf4j-client`, `rdf4j-storage`
e `slf4j-log4j12`. In aggiunta, sono stati dichiarati l'uso della codifica `UTF-8` e la compatibilità
con Java 8.

Nella cartella `src/main/resources` è stato agggiunto il file `log4j.properties` per configurare il
logger.

Ulteriore materiale
-------------------
Si consiglia di prendere visione anche del codice presentato negli anni precedenti (si presti attenzione al fatto che alcuni consigli potrebbero non essere più applicabili alle versioni pià recenti di RDF4J):
 
* https://bitbucket.org/art-uniroma2/teaching-ia2-2018_2019-lectures/src/master/ia2-2018_2019-rdf4j-exercises/
* https://bitbucket.org/art-uniroma2/teaching-ia2-2017_2018-lectures/src/master/rdf4j-examples/
* https://bitbucket.org/art-uniroma2/teaching-ia2-2016_2017-lectures/src/master/rdf4j-examples/

Breve introduzione ai costrutti di Java 8:

* https://bitbucket.org/art-uniroma2/teaching-ia2-2016_2017-lectures/src/master/java8-examples/

